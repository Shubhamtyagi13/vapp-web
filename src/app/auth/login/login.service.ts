import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LoginComponent } from './login.component';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    public dialog: MatDialog
  ) { }

  showLoginDialog(): void {
    this.dialog.open(LoginComponent, {
      width: '350px'
    });
  }
}
