import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyVRViewComponent } from './property-vrview.component';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    component: PropertyVRViewComponent
  }
];
@NgModule({
  declarations: [PropertyVRViewComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ]
})
export class PropertyVRViewModule { }
