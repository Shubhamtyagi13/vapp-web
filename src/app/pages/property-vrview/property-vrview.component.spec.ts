import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyVRViewComponent } from './property-vrview.component';

describe('PropertyVRViewComponent', () => {
  let component: PropertyVRViewComponent;
  let fixture: ComponentFixture<PropertyVRViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyVRViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyVRViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
