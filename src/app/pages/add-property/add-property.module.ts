import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddPropertyRoutingModule } from './add-property-routing.module';
import { AddPropertyComponent } from './add-property.component';
import { MatIconModule, MatStepperModule } from '@angular/material';

@NgModule({
  declarations: [AddPropertyComponent],
  imports: [
    CommonModule,
    AddPropertyRoutingModule,
    MatStepperModule, MatIconModule
  ]
})
export class AddPropertyModule { }
