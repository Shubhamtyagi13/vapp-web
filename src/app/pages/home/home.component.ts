import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  propertyList: any[] = [0, 1, 2];
  constructor(
    public route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.fragment.subscribe ( f => {
      const element: any = document.querySelector ( '#' + f );
      if (element) { window.scroll({left: 0, top: (element.offsetTop - 80), behavior: 'smooth'}); }
  });
  }

}
