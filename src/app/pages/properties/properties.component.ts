import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent implements OnInit {
  propertyList: any[] = [0, 1, 2, 3, 4, 5];
  constructor() { }

  ngOnInit() {
  }

}
