import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchResultComponent } from './search-result.component';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    component: SearchResultComponent
  }
];
@NgModule({
  declarations: [SearchResultComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ]
})
export class SearchResultModule { }
