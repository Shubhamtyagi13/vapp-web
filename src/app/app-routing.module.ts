import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGardService } from './common/services/auth-gard.service';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomeModule'
  },
  {
    path: 'search-result',
    loadChildren: './pages/search-result/search-result.module#SearchResultModule',
    canActivate: [AuthGardService]
  },
  {
    path: 'property-detail',
    loadChildren: './pages/property-detail/property-detail.module#PropertyDetailModule',
    canActivate: [AuthGardService]
  },
  {
    path: 'property-vr',
    loadChildren: './pages/property-vrview/property-vrview.module#PropertyVRViewModule',
    canActivate: [AuthGardService]
  },
  {
    path: 'profile',
    loadChildren: './pages/profile/profile.module#ProfileModule',
    canActivate: [AuthGardService]
  },
  {
    path: 'add-property',
    loadChildren: './pages/add-property/add-property.module#AddPropertyModule',
    canActivate: [AuthGardService]
  },
  {
    path: 'properties',
    loadChildren: './pages/properties/properties.module#PropertiesModule',
    canActivate: [AuthGardService]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
