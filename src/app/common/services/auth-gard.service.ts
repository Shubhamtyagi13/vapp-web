import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGardService implements CanActivate {

  constructor(
    public router: Router
  ) { }

  canActivate() {
    const user_info = localStorage.getItem('user_info');
    if (user_info) {
      return true;
    } else {
      // this.router.navigate(['/home']);
      return true;
    }
  }
}
